/*
Теоретичні питання:
1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
  Екранування - це додавання спецсимволів через зворотний слеш (символ екранування). Метод еранування використовується для створення рядків (перевід строки, додавання кавичок, зворотній слеш, ...) і зокрема для додавання в рядки символів через кодування Latin-1, UTF-16, UTF-32.

2. Які засоби оголошення функцій ви знаєте?
  function declaration - оголошуємо ім'я функції, список параметрів в круглих дужках, інструкції в фігурних дужках. Виклик функції працює над оголошенням і після оголошення.
  function expression - може бути анонімною (без iм'я); зручне оголошення, коли параметром функції є інша функція. Виклик функції працює тільки після оголошення.

3. Що таке hoisting, як він працює для змінних та функцій?
  Це мехінізм, в якому змінні та оголошення функцій піднімаються вверх своєї області видимості перед тим як код буде виконаний. Причому стосовно коду все лишається на своїх місцях, без змін. Це відбувається через те, що так працює контекст виконання в JavaScript.
*/

/*
Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
Створити метод getAge() який повертатиме скільки користувачеві років.
Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.
*/

function createNewUser() {
  let firstName = prompt("Введіть ім'я");
  let lastName = prompt('Введіть прізвище');
  let birthday = prompt('Введіть дату народження у форматі dd.mm.yyyy', 'dd.mm.yyyy');ж

  const newUser = {
    _firstName: firstName,
    _lastName: lastName,
    birthday: birthday,

    set firstName(value) {
      this._firstName = value;
    },
    get firstName() {
      return this._firstName;
    },
    set lastName(value) {
      this._lastName = value;
    },
    get lastName() {
      return this._lastName;
    },

    getLogin: function () {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },

    getAge: function () {
      birthday = birthday.split('.').reverse().join(',');
      let newUserBirthday = new Date(birthday);
      let now = new Date();
      let today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
      let newUserBirthdayInThisYear = new Date(today.getFullYear(),newUserBirthday.getMonth(),newUserBirthday.getDate());
      let age = today.getFullYear() - newUserBirthday.getFullYear();
      if (today < newUserBirthdayInThisYear) {
        age = age - 1;
      }
      return age;
    },

    getPassword: function () {
      return (this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6));
    },
  };

  return newUser;
}

let result = createNewUser();
console.log(result);
console.log(result.getAge());
console.log(result.getPassword());
